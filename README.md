<!---
SPDX-FileCopyrightText: 2024 Paul Brown <paul.brown@kde.org>
SPDX-License-Identifier: CC-BY-4.0
-->

# Spork

## Serial Port for KDE

This sample app will show the output from a sensor (thermometer, humidity sensor, light detector, etc.) hooked up to an Arduino-like device.

## Compiling

Install [QSerialPort](https://doc.qt.io/qt-5/qserialport.html)

```
sudo pacman -S qt6-serialport
```

From top directory, create the build directory:

```
cmake -B build
```

and then build the app with

```
cmake --build build/
```

## Run

Run from top directory with

```
build/bin/spork
```


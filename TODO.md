<!---
SPDX-FileCopyrightText: 2023 Paul Brown <paul.brown@kde.org>
SPDX-License-Identifier: CC-BY-4.0
-->

## TODO

* Overload == and != in a library (KSerialPort?) so we can easily compare port QSerialPortInfo objects
* Show stream from Serial port in tab

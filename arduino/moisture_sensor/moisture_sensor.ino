// SPDX-FileCopyrightText: 2023 Paul Brown <paul.brown@kde.org>
// SPDX-License-Identifier: CC0-1.0

#define AOUT_PIN A0 // Arduino pin that connects to AOUT pin of moisture sensor

void setup() {
  Serial.begin(9600);
}

void loop() {
  int value = analogRead(AOUT_PIN); // read the analog value from sensor

  Serial.println(value);

  delay(500);
}

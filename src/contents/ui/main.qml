// Includes relevant modules used by the QML
import QtQuick 2.15
import QtQuick.Controls 2.15 as Controls
import QtQuick.Layouts 1.15
import org.kde.kirigami 2.20 as Kirigami
// SPDX-FileCopyrightText: 2023 Paul Brown <paul.brown@kde.org>
// SPDX-License-Identifier: GPL-3.0-or-later

import org.kde.spork 1.0

// Provides basic features needed for all kirigami applications
Kirigami.ApplicationWindow {
    // Unique identifier to reference this object
    id: root

    // Window title
    // i18nc() makes a string translatable
    // and provides additional context for the translators
    title: i18nc("@title:window", "Spork")

    // Set the first page that will be loaded when the app opens
    // This can also be set to an id of a Kirigami.Page
    //    pageStack.initialPage: Kirigami.Page {
         // Add widgets here..
    globalDrawer: Kirigami.GlobalDrawer {

        header: Kirigami.AbstractApplicationHeader {

            Controls.Button {
                id: update
                icon.name: "view-refresh"
                //text: "Update"
                anchors.leftMargin: 10
                onClicked: Spork.updatePortList()
            }

            contentItem: Controls.ComboBox {
                id: availablePorts
                width: 200
                anchors.leftMargin: 2
                anchors.left: update.right
                model: Spork.portNames
                Layout.fillWidth: true
            }

            Controls.Button {
                id: select
                icon.name: "arrow-right"
                //text: "Update"
                anchors.leftMargin: 2
                anchors.left: availablePorts.right
                onClicked: Spork.selectPort(availablePorts.currentText)
            }

        }

        actions: [

                Kirigami.Action {
                    // Want to show the details (manufacturer, serial number, etc.) here
                    text: "Port Details"
                    },


                Kirigami.Action {
                    // I want to allow the user to configure the stream here
                    // * drop down for speed
                    // * checkbox for Autoscroll
                    // * checkbox for Timestamp
                    // * Button to clear output

                    text: "Configure stream"
                },
                Kirigami.Action {
                    text: i18n("Quit")
                    icon.name: "application-exit"
                    shortcut: StandardKey.Quit
                    onTriggered: Qt.quit()
                }
        ]
    }

    // I want the main action on two pages
    //  1. a texbox for a column of data incoming through the port

    pageStack.initialPage: Kirigami.Page {
        id: rawData
        Controls.Label {
            // Center label horizontally and vertically within parent object
            anchors.centerIn: parent
            text: i18n("Column of data")
        }
    }
    //  2. a line graph for numeric data
    Component {
        id: graphedData
        Kirigami.Page {
            Controls.Label {
                anchors.centerIn: parent
                text: i18n("Graph")
            }
        }
    }

}


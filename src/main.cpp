// SPDX-FileCopyrightText: 2023 Paul Brown <paul.brown@kde.org>
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KLocalizedContext>
#include <KLocalizedString>
#include <QApplication>
#include <QQmlApplicationEngine>
#include <QUrl>
#include <QtQml>

#include "spork.h"

int main(int argc, char* argv[])
{
    QApplication app(argc, argv);
    KLocalizedString::setApplicationDomain("learning");
    QCoreApplication::setOrganizationName(QStringLiteral("KDE"));
    QCoreApplication::setOrganizationDomain(QStringLiteral("kde.org"));
    QCoreApplication::setApplicationName(QStringLiteral("Spork"));

    Spork spork;
    qmlRegisterSingletonInstance<Spork>("org.kde.spork", 1, 0, "Spork", &spork);

    QFileSystemWatcher watcher;
    watcher.addPath(QStringLiteral("/dev"));

    QObject::connect(&watcher, SIGNAL(directoryChanged(QString)), &spork, SLOT(newPortAdded()));

    QQmlApplicationEngine engine;

    engine.rootContext()->setContextObject(new KLocalizedContext(&engine));
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    if (engine.rootObjects().isEmpty()) {
        return -1;
    }

    return app.exec();
}

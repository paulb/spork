// SPDX-FileCopyrightText: 2023 Paul Brown <paul.brown@kde.org>
// SPDX-License-Identifier: GPL-3.0-or-later

#include "spork.h"

namespace {
bool samePort(const QSerialPortInfo& portA, const QSerialPortInfo& portB)
{
    // Ports can be uniquely identified by manufacturer and serial number
    return portA.manufacturer() == portB.manufacturer() && portA.serialNumber() == portB.serialNumber();
}
}

Spork::Spork(QObject* parent)
    : QObject(parent)
{
    m_availablePorts = QSerialPortInfo::availablePorts();
}

void Spork::newPortAdded()
{
    QTimer::singleShot(30, this, &Spork::updatePortList); // When you connect a new USB device, you need to wait a bit for it to stabilise
}

void Spork::updatePortList()
{
    const auto newPorts = QSerialPortInfo::availablePorts();
    if (std::equal(newPorts.cbegin(), newPorts.cend(), m_availablePorts.cbegin(), samePort)) {
        return;
    }

    m_availablePorts = newPorts;
    Q_EMIT portsUpdated();
}

QList<QSerialPortInfo> Spork::getPortList()
{
    return m_availablePorts;
}

QStringList Spork::portNames() const
{
    QStringList portNames;
    for (const auto& port : m_availablePorts) {
        portNames << port.portName();
    }

    return portNames;
}

void Spork::selectPort(QString port)
{
    m_portinfo = QSerialPortInfo(port);
    // qDebug() << m_portinfo.portName() << " " << m_portinfo.productIdentifier();
    m_port.setPort(m_portinfo);
    // readPort(500);
    openPort();
}

bool Spork::openPort()
{
    try {
        m_port.open(QIODevice::ReadOnly);
        qDebug() << m_portinfo.portName() << " opened successfully.";
        m_port.clear();

        QTimer* timer = new QTimer(this);
        connect(timer, &QTimer::timeout, this, &Spork::readPort);
        timer->start(1000);

    } catch (...) {
        qDebug() << m_port.error();
        return false;
    }
    // m_port.close();
    return true;
}

bool Spork::readPort()
{
    // m_port.clear();

    qDebug() << m_port.readLine();
    return true;
}

bool Spork::closePort()
{
    try {
        m_port.close();
        qDebug() << m_portinfo.portName() << " closed successfully.";
        return true;
    } catch (...) {
        qDebug() << m_port.error();
        return false;
    }
}

// SPDX-FileCopyrightText: 2023 Paul Brown <paul.brown@kde.org>
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <QDebug>

#include <QFileSystemWatcher>
#include <QList>
#include <QObject>
#include <QSerialPort>
#include <QSerialPortInfo>
#include <QThread>
#include <QTimer>

class Spork : public QObject {
    Q_OBJECT
    Q_PROPERTY(QStringList portNames READ portNames NOTIFY portsUpdated)
    // Q_PROPERTY(QString select WRITE openPort NOTIFY portChanged)

public:
    explicit Spork(QObject* parent = nullptr);
    QList<QSerialPortInfo> getPortList();
    QStringList portNames() const;
    QSerialPortInfo portInfo(QString portName);
    bool openPort();
    bool readPort();
    bool closePort();

    // signals:
signals:
    void portsUpdated();

public slots:
    void updatePortList();
    void newPortAdded();
    void selectPort(QString port);

private:
    QList<QSerialPortInfo> m_availablePorts;
    QSerialPortInfo m_portinfo;
    QSerialPort m_port;
};
